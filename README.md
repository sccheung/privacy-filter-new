![Alt text](sys.jpg?raw=true "Usage of Privacy Filter")

DESCRIPTION
-----------

This is the Keras-based python implementation of the privacy preserving transformations 
presented in the following paper:

Cheung, S.-C., M. Usman Rafique, and W.-T. Tan. 2018. [**Privacy-Preserving Distributed Deep Learning with Privacy Transformation**](https://drive.google.com/file/d/1AqDz00qmglNYT_ptGZOfHSJUbfV9CWtN/view). 
In IEEE International Workshop on Information Forensics and Security (WIFS 2018), 11-13 December, 2018, Hong Kong, China.


USAGE
-----

1) Create two directories MNIST and CIFAR10
2) Edit ptrain.py to choose   
     (a) data set (comment/uncomment section)  
     (b) whether pretraining is used ("pretrained")  
   and run ptrain.py  
3) Edit ptransform.py to choose  
     (a) privacy transformation ("method")  
     (b) data set ("dset")  
     (c) public or private ("fname")  
   and run ptransform.py on BOTH public and private data
4) If reconstructor is used, edit reconstructor.py to choose  
     (a) privacy transformation ("privacy" see ptrasform.py for names)  
     (b) data set ("dset")  
     (c) public or private ("fname")  
5) Edit central-learner.py to choose  
     (a) privacy transformation ("privacy" see ptrasform.py for names)  
     (b) data set ("dset")  
     (c) use reconstructor netork ("use_recon")  
     (d) dense network or resnet ("use_denseNN")  

FILE DESCRIPTIONS
-----------------

1) ptrain.py

This program breaks the input data into two sets, public and private. 
If pretraining is desired, a pretrained model will be trained based on 
the public data and applied to both public and private datasets. 

Outputs: Notrans-public_data.pkl, Notrans-private_data.pkl, pmodel.hd5 (optional)  
Format:   
img_rows, img_cols = original image dimension. ex. 32, 32  
img_channels       = original image channel    ex. 3   
nb_classes         = original number or classe ex. 10  
classes_used       = labels used               ex. (0,1,3,4,6)  
pretrained         = is pretraining used?      ex. False  
privacy            = "Notrans"                 # as in no transformation  
x_train            = training feature  
y_train            = training labels  
x_test             = testing feature  
y_test             = testing labels  
ximg_train         = training image (scaled between 0 and 1)  
ximg_test          = testing image (scaled between 0 and 1)  

2) ptransform.py

This program applies different types of privacy preserving transformation on 
x_train and x_test produced in Step 1

Input: Notrans-public_data.npz or Notrans-private_data.npz  
Output: <method>-public/private_data.npz  
Format: same as that of ptrain.py except  
- privacy is the name of the privacy transformation used  
- x_train and x_test are replaced with the transformed feature  
- ximg_train and ximg_test are not saved  

3) reconstructor.py

This program uses the public data and the corresponding transformed data to 
build a reconstructor network to map the transformed data back to images. The
training data should be public data. It will report privacy measurements based 
on public testing data. 

Input:  Notrans-public_data.npz, <Privacy-Transformation>-public_data.npz  
Output: <Privacy-Transformation>-recon_model.h5  

4) central-learner.py

This program learns a central classifier based on a combined public and private
data. Optionally, it can use the reconstructor network built by reconstructor.py
to first map the input data back to image data. This is particularly useful 
for CNN-based central classifier which can only take images. If the reconstructor
network is used, it will also report privacy measurements on the private testing
data.  

Input:  <Privacy-Transformation>-public_data.npz  
        <Privacy-Transformation>-private_data.npz  
        <Privacy-Transformation>-recon_data.hd5  (optional)  
        Notrans-private data.npz (optional)  
Output: cmodel.hd5   

5) classifier.py

This contains routines that builds the classifer, which is used in both central
learning and pretraining, and fitting a model with helpful information displayed.

6) msssim.py 

Largely based on msssim.py downloaded from the URL below with some modification
https://github.com/tensorflow/models/blob/master/research/compression/image_encoder/msssim.py




