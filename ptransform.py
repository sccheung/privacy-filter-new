'''Privacy Transformation
   Author: Samson Cheung (sccheung@ieee.org)
   
This program applies different types of privacy preserving transformation on 
x_train and x_test produced in Step 1

Input: Notrans-public_data.npz or Notrans-private_data.npz
Output: <method>-public/private_data.npz
Format: 
img_rows, img_cols = original image dimension. ex. 32, 32
img_channels       = original image channel    ex. 3 
nb_classes         = original number or classe ex. 10
classes_used       = labels used               ex. (0,1,3,4,6)
method_name        = transformation used       ex. "RandDNN"
pretrained         = is pretraining used?      ex. False
x_train            = training feature
y_train            = training labels
x_test             = testing feature
y_test             = testing labels

'''

from __future__ import print_function
from pathlib import Path
import numpy as np
import enum
import os
import pickle
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.backends import default_backend

############################################################################
##  Definitions

# Supported Privacy Transforms
class Ptrans(enum.Enum):
    Notrans       = 0       # No transformation
    AddNoise      = 1       # Additive Noise
    CDMA          = 2       # Element-wise multiplication with noise vector
    Linear        = 3       # Random linear transformation
    Scramble      = 4       # Random permutation
    AES_ECB       = 5       # AES under ECB mode
    AES_CBC       = 6       # AES under CBC mode
    RandSNN       = 7       # SparseRandom Neural Network
    RandDNN       = 8       # Dense Random Neural Network

############################################################################
##  Input dataset, Outputs and Experimental conditions
method              = Ptrans.RandDNN
genNewKey           = False   # True if different keys are used for training and testing
dset                = "MNIST"
fname               = "private_data.pkl"
indata_fname        = dset+"/Notrans-"+fname
outdata_fname       = dset+"/"+method.name+"-"+fname

############################################################################
##  Read dataset
fp = open(indata_fname,"rb")
img_rows     = pickle.load(fp)
img_cols     = pickle.load(fp)
img_channels = pickle.load(fp)
nb_classes   = pickle.load(fp)
pretrained   = pickle.load(fp)
privacy_tmp  = pickle.load(fp)
x_train      = pickle.load(fp)
y_train      = pickle.load(fp)
x_test       = pickle.load(fp)
y_test       = pickle.load(fp)
ximg_train   = pickle.load(fp)
ximg_test    = pickle.load(fp)
fp.close()
            
############################################################################
##  Helper Routines

############################################3
# Dense Random Neural network
def randDNN(data,keyBankFile,
            numLayers   = 300, 
            genNewKey   = genNewKey): 
    # Normalize data so that they are between 0 and 1
    xmax      = np.max(data)
    xmin      = np.min(data)
    xrange    = xmax-xmin
    data      = (data-xmin)/xrange
    ##### Using Keras' layers take too much memory 10-12-2018
#    # Create secret parameters
#    if genNewKey or (not Path(keyBankFile).is_file()):
#        # Create a random dense network
#        dim     = data.shape[1]
#        inlayer = Input(shape=(dim,)) 
#        for i in range(numLayers):
#            if (i==0):
#                dense = Dense(units=dim,
#                              activation='selu',
#                              kernel_initializer = 'glorot_uniform',
#                              trainable=False)(inlayer)
#            else:
#                dense = Dense(units=dim,
#                              activation='selu',
#                              kernel_initializer = 'glorot_uniform',
#                              trainable=False)(dense)
#        model = Model(inputs=inlayer, outputs=dense)
#        model.save(keyBankFile)
#    else:
#        model = load_model(keyBankFile)    
#    # Apply the network 
#    model.summary()
#    data = model.predict(data)
    
    ##### Explicit implementation  10-12-2018
    data = 2*data-1
    dim  = data.shape[1]
    if genNewKey or (not Path(keyBankFile).is_file()):
        # Just 
        rstate  = np.random.get_state()
        fp      = open(keyBankFile,"wb")
        pickle.dump(rstate,fp)
        fp.close()
    else:
        fp      = open(keyBankFile,"rb")
        rstate  = pickle.load(fp)
        fp.close()
    # Apply the network: scaling based on glorot_uniform
    np.random.set_state(rstate)
    for i in range(numLayers):
        randM = (np.random.rand(dim,dim)-0.5)/np.sqrt(dim/6)
        randB = (np.random.rand(dim)-0.5)/np.sqrt(dim/6)
        data  = np.matmul(data,randM)+randB           # A = A*R+B
        data  = np.maximum(data,0)*2-1                # A = RELU(A)       
    print("Maximum output value of the random network = ",np.max(data))
    print("Minimum output value of the random network = ",np.min(data))

    # Normalize each feature
    xmax   = np.max(data,1)
    xmin   = np.min(data,1)
    xrange = xmax-xmin
    data   = np.transpose((data.transpose()-xmin)/xrange)     
    return data

############################################3
# Sparse unit-weight Neural network
def randSNN(data, keyBankFile,
            numLayers   = 50, 
            connect     = 0.1,
            genNewKey   = genNewKey):
    # Normalize data so that they are between 0 and 1
    xmax      = np.max(data)
    xmin      = np.min(data)
    xrange    = xmax-xmin
    data      = (data-xmin)/xrange
    data      = 2*data-1
    dim       = data.shape[1]
    if genNewKey or (not Path(keyBankFile).is_file()):
        # Just 
        rstate  = np.random.get_state()
        fp      = open(keyBankFile,"wb")
        pickle.dump(rstate,fp)
        fp.close()
    else:
        fp      = open(keyBankFile,"rb")
        rstate  = pickle.load(fp)
        fp.close()
    # Apply the network: scaling based on glorot_uniform
    np.random.set_state(rstate)
    connect  = int(dim*connect+0.5)
    for i in range(numLayers):
        randM = np.zeros((dim,dim))
        for j in range(dim):
            index = np.random.choice(range(0,dim),size=connect,replace=False)
            randM[j,index] = np.random.randint(low=-1,high=2,size=len(index))/np.sqrt(dim/6)
        randB = (np.random.rand(dim)*2-1)/np.sqrt(dim/6)
        data  = np.matmul(data,randM)+randB           # A = A*R+B
        data  = np.maximum(data,0)*2-1                # A = RELU(A)       
    print("Maximum output value of the random network = ",np.max(data))
    print("Minimum output value of the random network = ",np.min(data))        
       
    # Normalize for each feature
    xmax   = np.max(data,1)
    xmin   = np.min(data,1)
    xrange = xmax-xmin
    data   = np.transpose((data.transpose()-xmin)/xrange)     
    return data

############################################3
# AES
def AES(data, keyBankFile,
        mm          = Ptrans.AES_CBC,
        genNewKey   = genNewKey):
    
    # Quantize the data into 8-bit and pad the row length to be multiple of 16, as required by AES
    xmax      = np.max(data)
    xmin      = np.min(data)
    xrange    = xmax-xmin
    data      = np.floor((data-xmin)*255/xrange+0.5)
    if (data.shape[1]%16 > 0):
        tmp = np.zeros((data.shape[0],16-(data.shape[1]%16)))
        data = np.hstack((data,tmp))
    data      = data.astype('uint8')
    backend   = default_backend()
    
    #  Get key
    if genNewKey or  (not Path(keyBankFile).is_file()):
        key = os.urandom(16)    # 16 bytes = 128 bits for AES
        iv  = os.urandom(16) 
        np.savez(keyBankFile,a=np.array([key,iv]))
    else:
        tmpfz = np.load(keyBankFile)
        key = tmpfz['a'][0]
        iv  = tmpfz['a'][1] 
    
    m = modes.ECB() if (mm==Ptrans.AES_ECB) else modes.CBC(iv)              
    for i in range(data.shape[0]):
        plaintext = bytes(b'')
        cipher    = Cipher(algorithms.AES(key), m, backend=backend)
        encryptor = cipher.encryptor()
        for j in range(0,data.shape[1]):
            plaintext += int(data[i,j]).to_bytes(1,byteorder='big')          
        ciphertext = encryptor.update(plaintext) + encryptor.finalize()
        for j in range(0,data.shape[1]):
            data[i,j] = ciphertext[j]
            
    # Treat them as ordinary input
    data = data.astype('float32')
    data /= 255
    return data

############################################3
# Random Permutation
def scramble(data, keyBankFile,
             genNewKey = genNewKey):
    # Create secret parameters    
    if genNewKey or  (not Path(keyBankFile).is_file()):
        perm = np.random.permutation(data.shape[1])
        np.savez(keyBankFile,perm=perm)            
    else:
        tmpfz = np.load(keyBankFile)
        perm = tmpfz['perm'] 
        assert len(perm) == data.shape[1], "mismatch permutation in scramble"
    data = data[:,perm]    
    return data

############################################3
# Random Projection
def projection(data, keyBankFile,
               genNewKey = genNewKey):
    # Normalize data so that they are between 0 and 1
    xmax      = np.max(data)
    xmin      = np.min(data)
    xrange    = xmax-xmin
    data      = (data-xmin)/xrange
    # Create secret parameters    
    sz = data.shape[1]
    if genNewKey or  (not Path(keyBankFile).is_file()):
        rproj   = np.random.rand(sz,sz)-0.5
        np.savez(keyBankFile,rproj=rproj)
    else:
        tmpfz = np.load(keyBankFile)
        rproj = tmpfz['rproj']
        assert rproj.shape[0] == data.shape[1], "mismatch projection matrix in projection"
    data = np.inner(data,rproj)
    return data

############################################3
# CDMA
def CDMA(data, keyBankFile,
         genNewKey = genNewKey):
    # Normalize data so that they are between 0 and 1
    xmax      = np.max(data)
    xmin      = np.min(data)
    xrange    = xmax-xmin
    data      = (data-xmin)/xrange
    # Create secret parameters
    sz = data.shape[1]
    if genNewKey or  (not Path(keyBankFile).is_file()):
        cdma = np.random.randint(2,size=sz)*2-1
        s    = np.random.uniform(0,1,sz)
        np.savez(keyBankFile,cdma=cdma,s=s)
    else:
        tmpfz = np.load(keyBankFile)
        cdma = tmpfz['cdma']
        s    = tmpfz['s']
        assert len(s)==sz, "mismatch parameters in CDMA"
    data = ((data-0.5)*cdma+0.5+s)/2    

############################################3
# AddNoise
def addNoise(data, keyBankFile,
             genNewKey   = genNewKey, 
             binaryNoise = False):    
    # Normalize data so that they are between 0 and 1
    xmax      = np.max(data)
    xmin      = np.min(data)
    xrange    = xmax-xmin
    data      = (data-xmin)/xrange
    # Create secret parameters
    sz = data.shape[1]
    if genNewKey or  (not Path(keyBankFile).is_file()):
        s = np.random.randint(2,size=sz) if binaryNoise else np.random.uniform(-1,1,sz)
        np.savez(keyBankFile,s=s) 
    else:
        tmpfz = np.load(keyBankFile)
        s    = tmpfz['s']
        assert len(s)==sz, "mismatch parameters in addNoise"
    data = (data+s)/2 if binaryNoise else (data+s+1)/3     
    return data
    
############################################################################
##  Main Program
    
## Compute Privacy Transform on 1D feature
print("Computing Privacy Transformation "+method.name+" on the Training and Testing Data...")
outDir  = dset
privacy = method.name

# Method 8 : Uniform Random Neural network
if (method == Ptrans.RandDNN):
    ##### See modification in randDNN 10-12-2018
#    x_train = randDNN(x_train, keyBankFile = outDir+"/"+privacy+"-key.h5")
#    x_test  = randDNN(x_test,  keyBankFile = outDir+"/"+privacy+"-key.h5")
    x_train = randDNN(x_train, keyBankFile = outDir+"/"+privacy+"-key.pkl")
    x_test  = randDNN(x_test,  keyBankFile = outDir+"/"+privacy+"-key.pkl")
    
# Method 7: Random Neural Network
if (method == Ptrans.RandSNN):
    x_train = randSNN(x_train, keyBankFile = outDir+"/"+privacy+"-key.pkl")
    x_test  = randSNN(x_test,  keyBankFile = outDir+"/"+privacy+"-key.pkl")    
    
# Method 5 and 6: AES
if (method == Ptrans.AES_CBC) or (method == Ptrans.AES_ECB):
    x_train = AES(x_train, keyBankFile = outDir+"/"+privacy+"-key.npz", mm = method)  
    x_test  = AES(x_test,  keyBankFile = outDir+"/"+privacy+"-key.npz", mm = method)  
           
# Method 4: Random Permutation
if (method == Ptrans.Scramble):
    x_train = scramble(x_train, keyBankFile = outDir+"/"+privacy+"-key.npz")  
    x_test  = scramble(x_test,  keyBankFile = outDir+"/"+privacy+"-key.npz")  

# Method 3: Random Projection
if (method == Ptrans.Linear):
    x_train = projection(x_train, keyBankFile = outDir+"/"+privacy+"-key.npz")  
    x_test  = projection(x_test,  keyBankFile = outDir+"/"+privacy+"-key.npz")  
               
# Method 2: Add CDMA Noise
if (method == Ptrans.CDMA):
    x_train = CDMA(x_train, keyBankFile = outDir+"/"+privacy+"-key.npz")  
    x_test  = CDMA(x_test,  keyBankFile = outDir+"/"+privacy+"-key.npz") 

# Method 1:  Adding random binary noise
if (method == Ptrans.AddNoise):
    x_train = addNoise(x_train, keyBankFile = outDir+"/"+privacy+"-key.npz")  
    x_test  = addNoise(x_test,  keyBankFile = outDir+"/"+privacy+"-key.npz") 
    
# Save data
fp = open(outdata_fname,"wb") 
pickle.dump(img_rows,fp)
pickle.dump(img_cols,fp)
pickle.dump(img_channels,fp)
pickle.dump(nb_classes,fp)
pickle.dump(pretrained,fp)
pickle.dump(privacy,fp)
pickle.dump(x_train,fp)
pickle.dump(y_train,fp)
pickle.dump(x_test,fp)
pickle.dump(y_test,fp)
fp.close()




