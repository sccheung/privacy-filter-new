# -*- coding: utf-8 -*-
"""Preparing Input Data
   Author: Samson Cheung (sccheung@ieee.org)
   
   10/31/2018   Use pickle 4 to handle file > 4GB
                Incorporate data augmentation to enlarge dataset
                Separate pretraining 

This program breaks the input data into two sets, public and private. 
Optionally it will use image augmentation to add more training and testing data.

Outputs: Notrans-public_data.pkl, Notrans-private_data.pkl, pmodel.hd5 (optional)
Format: 
img_rows, img_cols = original image dimension. ex. 32, 32
img_channels       = original image channel    ex. 3 
nb_classes         = original number or classe ex. 10
classes_used       = labels used               ex. (0,1,3,4,6)
pretrained         = is pretraining used?      ex. False
privacy            = "Notrans"                 # as in no transformation
x_train            = training feature
y_train            = training labels
x_test             = testing feature
y_test             = testing labels
ximg_train         = training image (scaled between 0 and 1)
ximg_test          = testing image (scaled between 0 and 1)

"""

from __future__ import print_function
from keras.datasets import mnist, cifar10
from keras.utils import np_utils
import pickle
import numpy as np
from keras.preprocessing.image import ImageDataGenerator

############################################################################
##  Datasets Tested (uncomment only one set)

## MNIST Experiment
#(xorg_train, yorg_train), (xorg_test, yorg_test) = mnist.load_data()
#img_rows, img_cols = 28, 28
#img_channels       = 1
#nb_classes         = 10
##public_classes     = (0,1,2,3,4)
##private_classes    = (5,6,7,8,9)
##random_split       = False
#public_classes     = (0,1,2,3,4,5,6,7,8,9)
#private_classes    = public_classes
#random_split       = True
#dname              = "MNIST"
#use_denseNN        = True  
#dataExpanded       = 1         # Expand the dataset by this factor


# CIFAR-10 Experiment
(xorg_train, yorg_train), (xorg_test, yorg_test) = cifar10.load_data()
img_rows, img_cols = 32, 32
img_channels       = 3 
nb_classes         = 10
#public_classes     = (0,1,3,4,6)
#private_classes    = (2,5,7,8,9)
public_classes     = (0,1,2,3,4,5,6,7,8,9)
private_classes    = public_classes
random_split       = True
dname              = "CIFAR10"
use_denseNN        = False     # Use ResNet
dataExpanded       = 10        # Expand the dataset by this factor


############################################################################
## Parameters
private_fname       = dname+"/Notrans-private_data.pkl"
public_fname        = dname+"/Notrans-public_data.pkl"


############################################################################
## Process and save public and private data

# Turn labels into 1-D array
yorg_train = np.reshape(yorg_train,len(yorg_train))
yorg_test  = np.reshape(yorg_test,len(yorg_test))
models_built = False
if (use_denseNN):
    input_shape = (0,img_rows*img_cols*img_channels)
else:
    input_shape = (0,img_rows,img_cols,img_channels)
    
# public and private
is_sampled = False 
for [classes_used,fname] in [[public_classes,public_fname], [private_classes,private_fname]]:    
    # Training Data
    if not random_split:
        indx        = ()
        for each in classes_used:
            indx += ((yorg_train==each),)
        mask = np.logical_or.reduce(indx)
    else:
        if not is_sampled:
            dim         = xorg_train.shape[0]
            mask        = np.random.choice(range(dim),size=int(dim/2),replace=False)
            pmask_train = np.delete(np.array(range(dim)),mask)
        else:
            mask        = pmask_train        
    y_train     = np_utils.to_categorical(yorg_train[mask], nb_classes)
    ximg_train  = xorg_train[mask,]
    ximg_train  = ximg_train.astype('float32')
    ximg_train /=255
    ximg_train  = ximg_train.reshape((ximg_train.shape[0], img_rows, img_cols, img_channels))
    
    if (dataExpanded > 1):
        datagen = ImageDataGenerator(featurewise_center=False,  # set input mean to 0 over the dataset
                                     samplewise_center=False,  # set each sample mean to 0
                                     featurewise_std_normalization=False,  # divide inputs by std of the dataset
                                     samplewise_std_normalization=False,  # divide each input by its std
                                     zca_whitening=False,  # apply ZCA whitening
                                     rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
                                     width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
                                     height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
                                     horizontal_flip=True,  # randomly flip images
                                     vertical_flip=False)
        datagen.fit(ximg_train)
        iter = datagen.flow(ximg_train,y_train,batch_size=1000)
        tmpx = ximg_train
        tmpy = y_train
        sz   = dataExpanded*ximg_train.shape[0]
        while (tmpx.shape[0] <sz):
            x,y = iter.next()
            tmpx = np.vstack((tmpx,x))
            tmpy = np.vstack((tmpy,y))
        ximg_train = tmpx
        y_train    = tmpy
        
    
    # Testing Data
    if not random_split:
        indx        = ()    
        for each in classes_used:
            indx += ((yorg_test==each),)
        mask = np.logical_or.reduce(indx)
    else:
        if not is_sampled:
            is_sampled  = True
            dim         = xorg_test.shape[0]
            mask        = np.random.choice(range(dim),size=int(dim/2),replace=False)
            pmask_test  = np.delete(np.array(range(dim)),mask)
        else:
            mask        = pmask_test        
    y_test      = np_utils.to_categorical(yorg_test[mask], nb_classes)
    ximg_test   = xorg_test[mask,]          
    ximg_test   = ximg_test.astype('float32')
    ximg_test  /= 255
    ximg_test   = ximg_test.reshape((ximg_test.shape[0], img_rows, img_cols, img_channels))    

    if (dataExpanded > 1):
        datagen = ImageDataGenerator(featurewise_center=False,  # set input mean to 0 over the dataset
                                     samplewise_center=False,  # set each sample mean to 0
                                     featurewise_std_normalization=False,  # divide inputs by std of the dataset
                                     samplewise_std_normalization=False,  # divide each input by its std
                                     zca_whitening=False,  # apply ZCA whitening
                                     rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
                                     width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
                                     height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
                                     horizontal_flip=True,  # randomly flip images
                                     vertical_flip=False)
        datagen.fit(ximg_test)
        iter = datagen.flow(ximg_test,y_test,batch_size=1000)
        tmpx = ximg_test
        tmpy = y_test
        sz   = dataExpanded*ximg_test.shape[0]
        while (tmpx.shape[0] < sz):
            x,y = iter.next()
            tmpx = np.vstack((tmpx,x))
            tmpy = np.vstack((tmpy,y))
        ximg_test = tmpx
        y_test    = tmpy

    # No Pretraining
    dim     = np.prod(ximg_train.shape[1:])
    x_train = ximg_train.reshape(ximg_train.shape[0],dim)
    x_test  = ximg_test.reshape(ximg_test.shape[0],dim)

    
# Save data
fp = open(fname,"wb") 
pickle.dump(img_rows,fp,protocol=4)
pickle.dump(img_cols,fp,protocol=4)
pickle.dump(img_channels,fp,protocol=4)
pickle.dump(nb_classes,fp,protocol=4)
pickle.dump(False,fp,protocol=4)
pickle.dump("Notrans",fp,protocol=4)
pickle.dump(x_train,fp,protocol=4)
pickle.dump(y_train,fp,protocol=4)
pickle.dump(x_test,fp,protocol=4)
pickle.dump(y_test,fp,protocol=4)
pickle.dump(ximg_train,fp,protocol=4)
pickle.dump(ximg_test,fp,protocol=4)
fp.close()
    

    