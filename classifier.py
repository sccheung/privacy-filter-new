"""
Classifier and related routines 
Author: Samson Cheung (sccheung@ieee.org)

This contains routines that builds the classifer, which is used in both central
learning and pretraining, and fitting a model with helpful information displayed.


"""
from __future__ import print_function
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import np_utils
from keras.models import save_model, Model, Sequential, load_model
from keras.layers import Input, Dense, AlphaDropout, BatchNormalization, Dropout, Conv2D, LeakyReLU, Flatten
from keras.callbacks import ReduceLROnPlateau, EarlyStopping
import random
import numpy as np
import resnet
import matplotlib.pyplot as plt

############################################################################
# create_models
# For dense network (use_denseNN=True), input_shape should be (#samples,dimension)
# Otherwise, input_shape should be (#samples,img_cols,img_rols,img_channels)
# It returns both the central model and the pretrain model
def create_models(input_shape,                    # shape of the input tesnor
                  nb_classes,                     # number of classes
                  use_denseNN         = False,    # True = Dense; False = CNN
                  dropout             = 0.1,
                  dense_num_layer     = 8,        # Dense only: number of layers
                  dense_layer_dim     = 128):     # Dense only: dimension per layer
                 
    if (use_denseNN):
        # Flatten the data first
        inlayer  = Input(shape=(input_shape[1],)) 
        flatten1 = Dense(dense_layer_dim, 
                         activation='selu', 
                         kernel_initializer='lecun_normal')(inlayer)
        flatten1 = AlphaDropout(dropout)(flatten1)
        for i in range(dense_num_layer-1):
            flatten1 = Dense(dense_layer_dim, 
                             activation='selu', 
                             kernel_initializer='lecun_normal')(flatten1)
            flatten1 = AlphaDropout(dropout)(flatten1)
        pretrain_model = Model(inlayer, flatten1)
      
        dense = Dense(units=nb_classes, 
                      kernel_initializer="he_normal",
                      activation="softmax")(flatten1)
        central_model = Model(inlayer, dense) 


    else:    
        central_model,pretrain_model = resnet.ResnetBuilder.build_resnet_18((input_shape[3], 
                                                                             input_shape[1], 
                                                                             input_shape[2]),nb_classes)    
    pretrain_model.compile(loss='categorical_crossentropy',
                           optimizer='adam',
                           metrics=['accuracy'])    
    central_model.compile(loss='categorical_crossentropy',
                          optimizer='adam',
                          metrics=['accuracy'])
    
    return central_model, pretrain_model
    

############################################################################
# run_model
# Run any Keras model with default early stopper and plot to show training
# It returns the history output from model.fit.
def run_model(model,x_train,y_train,x_test,y_test,
              batch_size = 32,
              epochs     = 60,
              dataAugmented = False):
    lr_reducer    = ReduceLROnPlateau(monitor='val_loss',factor=np.sqrt(0.1), cooldown=0, patience=5, min_lr=0.5e-6)
    early_stopper = EarlyStopping(monitor='val_loss',min_delta=0, patience=20)
    
    if not dataAugmented:    
        history       = model.fit(x_train,y_train,
                                  batch_size       = batch_size,
                                  epochs           = epochs,
                                  validation_data  = (x_test,y_test),
                                  shuffle          = True,
                                  callbacks        = [lr_reducer, early_stopper])
    else:
        datagen = ImageDataGenerator(featurewise_center=False,  # set input mean to 0 over the dataset
                                     samplewise_center=False,  # set each sample mean to 0
                                     featurewise_std_normalization=False,  # divide inputs by std of the dataset
                                     samplewise_std_normalization=False,  # divide each input by its std
                                     zca_whitening=False,  # apply ZCA whitening
                                     rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
                                     width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
                                     height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
                                     horizontal_flip=True,  # randomly flip images
                                     vertical_flip=False)  # randomly flip images
        datagen.fit(x_train)
        history = model.fit_generator(datagen.flow(x_train, y_train, batch_size=batch_size),
                            steps_per_epoch=x_train.shape[0] // batch_size,
                            validation_data=(x_test, y_test),
                            epochs=epochs, verbose=1, max_q_size=100,
                            callbacks=[lr_reducer, early_stopper])
    plt.figure()
    plt.plot(history.epoch, history.history['val_acc'], 'g-', label='Network Val Acc')
    plt.plot(history.epoch, history.history['acc'],'g--', label='Network Acc')
    plt.xlabel('Epochs')
    plt.ylabel('Acc')
    plt.legend()
    plt.show(block=False)    
    return history