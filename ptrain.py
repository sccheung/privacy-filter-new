# -*- coding: utf-8 -*-
"""Preparing and Pretraing Input Data
   Author: Samson Cheung (sccheung@ieee.org)

This program breaks the input data into two sets, public and private. 
If pretraining is desired, a pretrained model will be trained based on 
the public data and applied to both public and private datasets. 

Outputs: Notrans-public_data.pkl, Notrans-private_data.pkl, pmodel.hd5 (optional)
Format: 
img_rows, img_cols = original image dimension. ex. 32, 32
img_channels       = original image channel    ex. 3 
nb_classes         = original number or classe ex. 10
classes_used       = labels used               ex. (0,1,3,4,6)
pretrained         = is pretraining used?      ex. False
privacy            = "Notrans"                 # as in no transformation
x_train            = training feature
y_train            = training labels
x_test             = testing feature
y_test             = testing labels
ximg_train         = training image (scaled between 0 and 1)
ximg_test          = testing image (scaled between 0 and 1)

"""

from __future__ import print_function
from keras.datasets import mnist, cifar10
from keras.utils import np_utils
from keras.models import save_model, Model
import pickle
import numpy as np
import classifier

############################################################################
##  Datasets Tested (uncomment only one set)

## MNIST Experiment
#(xorg_train, yorg_train), (xorg_test, yorg_test) = mnist.load_data()
#img_rows, img_cols = 28, 28
#img_channels       = 1
#nb_classes         = 10
##public_classes     = (0,1,2,3,4)
##private_classes    = (5,6,7,8,9)
##random_split       = False
#public_classes     = (0,1,2,3,4,5,6,7,8,9)
#private_classes    = public_classes
#random_split       = True
#dname              = "MNIST"
#use_denseNN        = True 
#dataAugmented      = False      


# CIFAR-10 Experiment
(xorg_train, yorg_train), (xorg_test, yorg_test) = cifar10.load_data()
img_rows, img_cols = 32, 32
img_channels       = 3 
#nb_classes         = 10
#public_classes     = (0,1,3,4,6)
#private_classes    = (2,5,7,8,9)
public_classes     = (0,1,2,3,4,5,6,7,8,9)
private_classes    = public_classes
random_split       = True
dname              = "CIFAR10"
use_denseNN        = False     # Use ResNet
dataAugmented      = True


############################################################################
## Parameters
private_fname       = dname+"/Notrans-private_data.pkl"
public_fname        = dname+"/Notrans-public_data.pkl"
pmodel_fname        = dname+"/pmodel.hd5"
pretrained          = False



############################################################################
## Process and save public and private data

# Turn labels into 1-D array
yorg_train = np.reshape(yorg_train,len(yorg_train))
yorg_test  = np.reshape(yorg_test,len(yorg_test))
models_built = False
if (use_denseNN):
    input_shape = (0,img_rows*img_cols*img_channels)
else:
    input_shape = (0,img_rows,img_cols,img_channels)
    
# public and private
is_sampled = False 
for [classes_used,fname] in [[public_classes,public_fname], [private_classes,private_fname]]:    
    # Training Data
    if not random_split:
        indx        = ()
        for each in classes_used:
            indx += ((yorg_train==each),)
        mask = np.logical_or.reduce(indx)
    else:
        if not is_sampled:
            dim         = xorg_train.shape[0]
            mask        = np.random.choice(range(dim),size=int(dim/2),replace=False)
            pmask_train = np.delete(np.array(range(dim)),mask)
        else:
            mask        = pmask_train        
    y_train     = np_utils.to_categorical(yorg_train[mask], nb_classes)
    ximg_train  = xorg_train[mask,]
    ximg_train  = ximg_train.astype('float32')
    ximg_train /=255
    ximg_train  = ximg_train.reshape((ximg_train.shape[0], img_rows, img_cols, img_channels))
    
    # Testing Data
    if not random_split:
        indx        = ()    
        for each in classes_used:
            indx += ((yorg_test==each),)
        mask = np.logical_or.reduce(indx)
    else:
        if not is_sampled:
            is_sampled  = True
            dim         = xorg_test.shape[0]
            mask        = np.random.choice(range(dim),size=int(dim/2),replace=False)
            pmask_test  = np.delete(np.array(range(dim)),mask)
        else:
            mask        = pmask_test        
    y_test      = np_utils.to_categorical(yorg_test[mask], nb_classes)
    ximg_test   = xorg_test[mask,]          
    ximg_test   = ximg_test.astype('float32')
    ximg_test  /= 255
    ximg_test   = ximg_test.reshape((ximg_test.shape[0], img_rows, img_cols, img_channels))    

    # No Pretraining
    dim     = np.prod(ximg_train.shape[1:])
    x_train = ximg_train.reshape(ximg_train.shape[0],dim)
    x_test  = ximg_test.reshape(ximg_test.shape[0],dim)

    if pretrained:   
        # Pretraining
        # First build the model using public data (built only once!)
        if not models_built:
            models_built   = True
            cmodel, pmodel = classifier.create_models(input_shape,nb_classes,
                                                      use_denseNN = use_denseNN)
            cmodel.summary()
            if (use_denseNN):
                classifier.run_model(cmodel, x_train, y_train, x_test, y_test)
            else:
                classifier.run_model(cmodel, ximg_train, y_train, ximg_test, y_test,
                                     dataAugmented = dataAugmented)
            pmodel.save(pmodel_fname)

        # Then, apply pretraining to both public and private
        if (use_denseNN):           
            x_train = pmodel.predict(x_train)
            x_test  = pmodel.predict(x_test)
        else:
            x_train = pmodel.predict(ximg_train)
            x_test  = pmodel.predict(ximg_test)
    
    # Save data
    fp = open(fname,"wb") 
    pickle.dump(img_rows,fp)
    pickle.dump(img_cols,fp)
    pickle.dump(img_channels,fp)
    pickle.dump(nb_classes,fp)
    pickle.dump(pretrained,fp)
    pickle.dump("Notrans",fp)
    pickle.dump(x_train,fp)
    pickle.dump(y_train,fp)
    pickle.dump(x_test,fp)
    pickle.dump(y_test,fp)
    pickle.dump(ximg_train,fp)
    pickle.dump(ximg_test,fp)
    fp.close()
    

    