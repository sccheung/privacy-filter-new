"""
Central Learner 
Author: Samson Cheung (sccheung@ieee.org)

This program learns a central classifier based on a combined public and private
data. Optionally, it can use the reconstructor network built by reconstructor.py
to first map the input data back to image data. This is particularly useful 
for CNN-based central classifier which can only take images. If the reconstructor
network is used, it will also report privacy measurements on the private testing
data.  

Input:  <Privacy-Transformation>-public_data.npz
        <Privacy-Transformation>-private_data.npz
        <Privacy-Transformation>-recon_data.hd5  (optional)
        Notrans-private data.npz (optional)
Output: cmodel.hd5 

"""
from __future__ import print_function
from keras.models import save_model, Model, Sequential, load_model
import numpy as np
import pickle
import classifier
import matplotlib.pyplot as plt

############################################################################
##  Input dataset and Outputs
dset                = "CIFAR10"
privacy             = "RandDNN"
use_denseNN         = False    # True = Dense; False = CNN
use_recon           = True     # Apply reconstructor 
recon_model_fname   = dset+"/"+privacy+"-recon_model.hd5"
save_cmodel         = True     # save learned centralized model
cmodel_fname        = dset+"/cmodel.hd5"
dataAugmented       = True

############################################################################
##  Read both public and private datasets
fp = open(dset+"/"+privacy+"-public_data.pkl","rb")
img_rows     = pickle.load(fp)
img_cols     = pickle.load(fp)
img_channels = pickle.load(fp)
nb_classes   = pickle.load(fp)
pretrained   = pickle.load(fp)
privacy1     = pickle.load(fp)
assert (privacy==privacy1),             "Expect "+privacy+" but read "+privacy1
x_train      = pickle.load(fp)
y_train      = pickle.load(fp)
x_test       = pickle.load(fp)
y_test       = pickle.load(fp)
fp.close()

fp = open(dset+"/"+privacy+"-private_data.pkl","rb")
img_rows1     = pickle.load(fp)
assert (img_rows==img_rows1),           "Mismatch between two input datasets"
img_cols1     = pickle.load(fp)
assert (img_cols==img_cols1),           "Mismatch between two input datasets"
img_channels1 = pickle.load(fp)
assert (img_channels==img_channels1),   "Mismatch between two input datasets"
nb_classes1  = pickle.load(fp)
assert (nb_classes==nb_classes1),       "Mismatch between two input datasets"
pretrained1  = pickle.load(fp)
assert (pretrained==pretrained1),       "Mismatch between two input datasets"
privacy1     = pickle.load(fp)
assert (privacy==privacy1),             "Expect "+privacy+" but read "+privacy1
x_train_pr   = pickle.load(fp)
y_train_pr   = pickle.load(fp)
x_test_pr    = pickle.load(fp)
y_test_pr    = pickle.load(fp)
fp.close()

############################################################################
##  Reconstructor
if use_recon:
    reconstructor = load_model(recon_model_fname)
    x_train       = reconstructor.predict(x_train)
    x_train_pr    = reconstructor.predict(x_train_pr)
    x_test        = reconstructor.predict(x_test)
    x_test_pr     = reconstructor.predict(x_test_pr)

############################################################################
##  Merging two databases 
if use_denseNN:
    x_train    = np.append(x_train.reshape(x_train.shape[0],np.prod(x_train.shape[1:])),
                           x_train_pr.reshape(x_train_pr.shape[0],np.prod(x_train_pr.shape[1:])),
                           axis=0)
    x_test     = np.append(x_test.reshape(x_test.shape[0],np.prod(x_test.shape[1:])),
                           x_test_pr.reshape(x_test_pr.shape[0],np.prod(x_test_pr.shape[1:])),
                           axis=0)
else:
    x_train = np.append(x_train,x_train_pr,axis=0)
    x_test  = np.append(x_test,x_test_pr,axis=0)
    assert(len(x_train.shape)==4), "ResNet can only take image data but the inputs are not."
    
y_train = np.append(y_train,y_train_pr,axis=0)
y_test  = np.append(y_test,y_test_pr,axis=0)

############################################################################
##  Centralized Learning               
cmodel, pmodel = classifier.create_models(x_train.shape,nb_classes,
                                          use_denseNN = use_denseNN)
cmodel.summary()
classifier.run_model(cmodel, x_train, y_train, x_test, y_test, 
                     dataAugmented = dataAugmented and use_recon)
if (save_cmodel):
    cmodel.save(cmodel_fname)
    

############################################################################
##  Read Private Image Dataset and perform privacy measurement on the 
##  reconstructor model, which should be trained on public data only
if not use_recon:
    exit
fp = open(dset+"/Notrans-private_data.pkl","rb")
img_rows1     = pickle.load(fp)
assert (img_rows==img_rows1),           "Mismatch between feature and image datasets"
img_cols1     = pickle.load(fp)
assert (img_cols==img_cols1),           "Mismatch between feature and image datasets"
img_channels1 = pickle.load(fp)
assert (img_channels==img_channels1),   "Mismatch between feature and image datasets"
nb_classes1  = pickle.load(fp)
assert (nb_classes==nb_classes1),       "Mismatch between feature and image datasets"
pretrained1  = pickle.load(fp)
assert (pretrained==pretrained1),       "Mismatch between feature and image datasets"
privacy1     = pickle.load(fp)
assert (privacy1=="Notrans"),           "Expect Notrans but read "+privacy1
x_train      = pickle.load(fp)
y_train      = pickle.load(fp)
x_test       = pickle.load(fp)
y_test       = pickle.load(fp)
ximg_train   = pickle.load(fp)
ximg_test    = pickle.load(fp)

############################################################################
## Show original images
indx = np.random.choice(range(ximg_test.shape[0]),
                        size=8,replace=False)  # 8 Random Images to show before and after PPT
print("Originals:")
sz = img_rows*img_cols
f, axarr = plt.subplots(1, len(indx),figsize=(20,10))
for i in range(len(indx)):  
    if (img_channels>1):
        tmp = []
        for j in range(img_channels):
            tmp.append(ximg_test[indx[i],:,:,j])      
        axarr[i].imshow(np.dstack(tuple(tmp)))
    else:
        axarr[i].imshow(ximg_test[indx[i],:].reshape(img_rows,img_cols))       
plt.show(block=False)

############################################################################
## Show reconstructed images
print("Reconstructed:")
f, axarr = plt.subplots(1, len(indx),figsize=(20,10))
for i in range(len(indx)):
    if (img_channels>1):
        tmp = []
        for j in range(img_channels):
            tmp.append(x_test_pr[indx[i],:,:,j])       
        axarr[i].imshow(np.dstack(tuple(tmp)))
    else:
        axarr[i].imshow(x_test_pr[indx[i],:].reshape(img_rows,img_cols))
plt.show(block=False)

############################################################################
## Privacy measurement
mean_recon,std_recon,mean_PSNR,std_PSNR,mean_msssim,std_msssim = msssim.compare_two_imagesets(ximg_test,x_test_pr)
print("Rec vs Mean(Rec) PSNR: ",mean_recon,  " +- ",std_recon, " (big means better privacy)")
print("Rec vs Original  PSNR: ",mean_PSNR,   " +- ",std_PSNR,  " (small means better privacy)")
print("Rec vs Original  MSIM: ",mean_msssim, " +- ",std_msssim," (small means better privacy)")


    


