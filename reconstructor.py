# -*- coding: utf-8 -*-
"""Build Reconstruction Network
   Author: Samson Cheung (sccheung@ieee.org)
   
This program uses the public data and the corresponding transformed data to 
build a reconstructor network to map the transformed data back to images. The
training data should be public data. It will report privacy measurements based 
on public testing data. 

Input:  Notrans-public_data.npz, <Privacy-Transformation>-public_data.npz
Output: <Privacy-Transformation>-recon_model.h5


"""

from __future__ import print_function
import keras
from keras.utils import np_utils
from keras.models import save_model, Model, Sequential, load_model
from keras.layers import Input, Dense, AlphaDropout, BatchNormalization, Dropout, Conv2D, Flatten, Reshape, Conv2DTranspose
import numpy as np
import pickle
import matplotlib.pyplot as plt
import msssim
import classifier

############################################################################
##  Input datasets, Outputs and Experimental conditions
dset                = "MNIST"
fname               = "public_data.pkl"
privacy             = "RandDNN"
imgdata_fname       = dset+"/"+"Notrans-"+fname
fdata_fname         = dset+"/"+privacy+"-"+fname
recon_model_fname   = dset+"/"+privacy+"-recon_model.hd5"

############################################################################
##  Read image database first
fp = open(imgdata_fname,"rb")
img_rows     = pickle.load(fp)
img_cols     = pickle.load(fp)
img_channels = pickle.load(fp)
nb_classes   = pickle.load(fp)
pretrained   = pickle.load(fp)
privacy1     = pickle.load(fp)
x_train      = pickle.load(fp)
y_train      = pickle.load(fp)
x_test       = pickle.load(fp)
y_test       = pickle.load(fp)
ximg_train   = pickle.load(fp)
ximg_test    = pickle.load(fp)
fp.close()

############################################################################
##  Read feature database next
fp = open(fdata_fname,"rb")
img_rows1     = pickle.load(fp)
assert (img_rows==img_rows1),           "Mismatch between two input datasets"
img_cols1     = pickle.load(fp)
assert (img_cols==img_cols1),           "Mismatch between two input datasets"
img_channels1 = pickle.load(fp)
assert (img_channels==img_channels1),   "Mismatch between two input datasets"
nb_classes1  = pickle.load(fp)
assert (nb_classes==nb_classes1),       "Mismatch between two input datasets"
pretrained1  = pickle.load(fp)
assert (pretrained==pretrained1),       "Mismatch between two input datasets"
privacy1     = pickle.load(fp)
assert (privacy==privacy1),             "Expect "+privacy+" but read "+privacy1
x_train      = pickle.load(fp)
assert (len(x_train)==len(ximg_train)), "Mismatch between two input datasets"
y_train      = pickle.load(fp)
x_test       = pickle.load(fp)
y_test       = pickle.load(fp)
fp.close()


############################################################################
# Build reconstructor model and apply to test images
dim                 = x_train.shape[1]
core_dim            = 1024
num_dense_layers    = 2
cnn_filters         = 32
stride              = 2
filter_sz           = 3
batch_size          = 32
nb_epoch            = 60
dropout             = 0.1


# Dense network in reducing dimesion to 64
inlayer = Input(shape=(dim,))
x = Dense(core_dim*num_dense_layers,
          activation='selu',
          kernel_initializer='lecun_normal')(inlayer)
x = AlphaDropout(dropout)(x)
for i in range(num_dense_layers-1):
    x = Dense(core_dim*(num_dense_layers-1-i),
          activation='selu',
          kernel_initializer='lecun_normal')(x)              
    x = AlphaDropout(dropout)(x)
    
# Expand to fit in to Conv2DTranspose
outshape = (int(img_rows/stride),int(img_cols/stride),cnn_filters)
#outshape = (int(img_rows/stride/stride),int(img_cols/stride/stride),cnn_filters)   
x = Dense(np.prod(outshape),
          activation='selu',
          kernel_initializer='lecun_normal')(x)
x = Reshape(outshape)(x)
    
# Spatial Interrpolation
#x = Conv2DTranspose(cnn_filters, filter_sz,
#                    padding='same',
#                    activation='relu',
#                    strides=(stride,stride))(x)

# Spatial Interpolation
x = Conv2DTranspose(cnn_filters, filter_sz,
                    padding='same',
                    activation='relu',
                    strides=(stride,stride))(x)

x = Conv2D(img_channels,filter_sz,
           padding='same',
           activation='sigmoid')(x)
reconstructor = Model(inlayer,x)
reconstructor.compile(loss='mse',
                      optimizer='adam',
                      metrics=['accuracy'])    
reconstructor.summary()

# Training
ximg_train    = ximg_train.reshape((x_train.shape[0], img_rows, img_cols, img_channels))
ximg_test     = ximg_test.reshape((x_test.shape[0], img_rows, img_cols, img_channels))
classifier.run_model(reconstructor, x_train, ximg_train, x_test, ximg_test)
reconstructor.save(recon_model_fname)


############################################################################
## Show original images
indx = np.random.choice(range(ximg_test.shape[0]),
                        size=8,replace=False)  # 8 Random Images to show before and after PPT
print("Originals:")
sz = img_rows*img_cols
f, axarr = plt.subplots(1, len(indx),figsize=(20,10))
for i in range(len(indx)):  
    if (img_channels>1):
        tmp = []
        for j in range(img_channels):
            tmp.append(ximg_test[indx[i],:,:,j])      
        axarr[i].imshow(np.dstack(tuple(tmp)))
    else:
        axarr[i].imshow(ximg_test[indx[i],:].reshape(img_rows,img_cols))       
plt.show(block=False)

############################################################################
## Show reconstructed images
recon_test = reconstructor.predict(x_test)
print("Reconstructed:")
f, axarr = plt.subplots(1, len(indx),figsize=(20,10))
for i in range(len(indx)):
    if (img_channels>1):
        tmp = []
        for j in range(img_channels):
            tmp.append(recon_test[indx[i],:,:,j])       
        axarr[i].imshow(np.dstack(tuple(tmp)))
    else:
        axarr[i].imshow(recon_test[indx[i],:].reshape(img_rows,img_cols))
plt.show(block=False)


############################################################################
## Privacy measurement
mean_recon,std_recon,mean_PSNR,std_PSNR,mean_msssim,std_msssim = msssim.compare_two_imagesets(ximg_test,recon_test)
print("Rec vs Mean(Rec) PSNR: ",mean_recon,  " +- ",std_recon, " (big means better privacy)")
print("Rec vs Original  PSNR: ",mean_PSNR,   " +- ",std_PSNR,  " (small means better privacy)")
print("Rec vs Original  MSIM: ",mean_msssim, " +- ",std_msssim," (small means better privacy)")

